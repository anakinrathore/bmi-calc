import React, { Component } from 'react';
import './App.css';
import logo from './logo.svg';
import BMICalculator from "./components/BMICalculator";

class App extends Component {
  render() {
    return (
      <div className="BMIView">

          <header className="App-header">
            <a
                className="App-link"
                href="#"
                target="_blank"
                rel="noopener noreferrer"
            >
              BMI Calculator
            </a>
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              <BMICalculator />
            </p>

          </header>
      </div>
    );
  }
}

export default App;
