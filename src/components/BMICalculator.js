import React, {Component} from 'react';

export default class BMICalculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bmi: '',
            height: "",
            weight: '',
        };
    }

    controlHeight = (evt) => {
        this.setState({height: evt.target.value})
    };
    controlWeight = (e) => {
        this.setState({weight: e.target.value})
    }

    render() {
        return (<div align='center ' className={'bmiCalculator'}>
            <label htmlFor="height">Enter height : </label>
            <input type="number" placeholder='enter height in meters' required className='height'
                   value={this.state.height} onChange={this.controlHeight}/><br/>
            <label htmlFor="weight">Enter weight : </label>
            <input type="number" placeholder='enter weight in kilograms' required className='weight'
                   value={this.state.weight} onChange={this.controlWeight}/><br/>
            <button
                onClick={() => this.setState({bmi: (parseFloat(this.state.weight) / (parseFloat(this.state.height) ** 2))})}
                className='calculateBMI'>Calculate
            </button>
            <br/>
            <span className='result'>{this.state.bmi}</span>
        </div>);
    }
}