import React from 'react';
import '../../setupTests';
import ReactDOM from 'react-dom';
import App from '../App';

describe('BMI', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});