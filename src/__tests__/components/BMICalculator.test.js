import React from 'react';
import '../../../setupTests';
import {mount} from "enzyme";
import BMICalculator from "../../components/BMICalculator";

describe('BMI', () => {
    it('renders the input field for height', () => {
        const documentRoot = mount(<BMICalculator/>);
        expect(documentRoot.exists('input.height')).toBeTruthy();

    });

    it('renders the input field for weight', () => {
        const documentRoot = mount(<BMICalculator/>);
        expect(documentRoot.exists('input.weight')).toBeTruthy();

    });

    it('renders the button for calculating the BMI', () => {
        const documentRoot = mount(<BMICalculator/>);
        expect(documentRoot.exists('button.calculateBMI')).toBeTruthy();
    });

    it('renders the span area for result', () => {
        const documentRoot = mount(<BMICalculator/>);
        expect(documentRoot.exists('span.result')).toBeTruthy();

    });

    it('expects the value entered in weight input field', () => {
        const documentRoot = mount(<BMICalculator/>);
        const weightInputField = documentRoot.find('input.weight').instance();
        weightInputField.value = 70;
        expect(weightInputField.value).toBe('70');

    });

    it('expects the value entered in weight and height input', () => {
        const documentRoot = mount(<BMICalculator/>);
        const weightInputField = documentRoot.find('input.weight').instance();
        const heightInputField = documentRoot.find('input.height').instance();
        heightInputField.value = 10;
        weightInputField.value = 100;
        expect(weightInputField.value).toBe('100');
        expect(heightInputField.value).toBe('10');

    });

    it('expects the result in the result span', () => {
        const documentRoot = mount(<BMICalculator/>);
        const weightInputField = documentRoot.find('input.weight');
        const heightInputField = documentRoot.find('input.height');
        const calculateButton = documentRoot.find('button.calculateBMI');
        heightInputField.simulate('change', {target: {value: 10}});
        weightInputField.simulate('change', {target: {value: 100}});
        calculateButton.simulate("click");
        expect(documentRoot.find('span.result').text()).toBe("1");
    });
});